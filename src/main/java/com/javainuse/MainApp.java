package com.javainuse;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainApp {

  public static void main(String[] args) {
    AbstractApplicationContext ctx =
        new ClassPathXmlApplicationContext("applicationContext-camel.xml");
    ctx.start();
    System.out.println("Application context started");
    try {
      Thread.sleep(5 * 60 * 1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    ctx.stop();
    ctx.close();
  }

  public static void mainSimple(String[] args) {
    SimpleRouteBuilder routeBuilder = new SimpleRouteBuilder();
    CamelContext ctx = new DefaultCamelContext();
    try {
      ctx.addRoutes(routeBuilder);
      ctx.start();
      Thread.sleep(5 * 60 * 1000);
      ctx.stop();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
