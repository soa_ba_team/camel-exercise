package com.javainuse;

import org.apache.camel.builder.RouteBuilder;

public class SpringSimpleRouteBuilder extends RouteBuilder {

  @Override
  public void configure() throws Exception {
    from("file:/home/luismuzikant/development/camel/inputFolder?delete=true")
        .process(new MyProcessor())
        .to("file:/home/luismuzikant/development/camel/outputFolder");
  }
}
